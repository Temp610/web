from django import forms

class QuestionForm(forms.Form):
    title = forms.CharField(max_length=60)
    text = forms.Textarea()
    tags = forms.CharField(max_length=200)

class RegistrationForm(forms.Form):
    login = forms.CharField(label='Login', max_length=100)
    email = forms.EmailField(label='Email', max_length=100)
    password = forms.CharField(label='Password', max_length=50)
	
