from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from collections import OrderedDict
from ask.forms import *
import random
import json

from ask.models import *

class AjaxResponse(HttpResponse):
    
    def __init__(self, status, msg, extra_context=None):

	response = {'status': status, 'msg': msg}
	
	if not extra_context is None:
	    response.update(extra_context)

	super(AjaxResponse, self).__init__(
	    content=json.dumps(response),
	    content_type='application/json'
	)


def user(request, user_id):
    currentuser = Profile.objects.get(pk=user_id)
    return render(request, 'user.html', {
	'currentuser': currentuser,
	'best_tags': Tag.objects.all().order_by('rating').reverse()[:10],
	'best_users': Profile.objects.all().order_by('rating').reverse()[:10],
    })

def getPageDict(paginator, page):
   pages = {}
   if paginator.num_pages > 5:
	if int(page) < 4:
	    pages = { 1: 1, 2: 2, 3: 3, 4: 4, '...': 5, paginator.num_pages: 6 }
	if int(page) > int(paginator.num_pages - 3):
	    pages = { 1: 1, '...': 2, paginator.num_pages - 3: 3, paginator.num_pages - 2: 4, paginator.num_pages - 1: 5, paginator.num_pages: 6 }
	if int(page) > 3 and int(page) < paginator.num_pages - 2:
	    pages = { 1: 1, '..': 2, int(page) - 1: 3, int(page): 4, int(page) + 1: 5, '...': 6, paginator.num_pages: 7}
	pages = OrderedDict(sorted(pages.items(), key=lambda t: t[1]))     
   else:
	pages = paginator.page_range
   if paginator.num_pages == 1:
	pages = {}
   return pages
	
    

def question(request, question_id):
    
    try:
	question = Question.objects.get(pk=question_id)
	answers = question.answer_set.all().order_by('date_added')
    except:
	raise Http404

    page = request.GET.get('page', 1)
    paginator = Paginator(answers, 3)

    try:
	currentpage = paginator.page(page)
    except EmptyPage:
	raise Http404
    except PageNotAnInteger:
	raise Http404

    if request.method == "POST":
        if len(request.POST['text']) >= 10:
             Answer.objects.create(author=request.user.profile,
                            question=question,
                            text=request.POST['text'])
             return redirect(request.get_full_path())
	else:
	     return redirect(request.get_full_path()) # TODO: text_length_error!
    
    return render(request, 'question.html', {
	'question' : question,
	'best_tags': Tag.objects.all().order_by('rating').reverse()[:10],
	'best_users': Profile.objects.all().order_by('rating').reverse()[:10],
	'pages': getPageDict(paginator, page),
	'answers' : paginator.page(page),
    })

def index(request):
  
    questions = ''

    if request.is_ajax():

        page = request.REQUEST.get('page', 1)
        sort = request.REQUEST.get('sort', 'new')
        tag = request.REQUEST.get('tag', '')

        if sort == 'new':
	    sorting = 'date_added'
	    if tag != '':
	        questions = Tag.objects.get(name=tag).question_set.order_by(sorting).reverse()
	    else:
                questions = Question.objects.order_by(sorting).reverse()
        	
        if sort == 'hot':
            sorting = 'rating'
            if tag != '':
                questions = Tag.objects.get(name=tag).question_set.order_by(sorting).reverse()
            else:
                questions = Question.objects.order_by(sorting).reverse()
        
        if sort == 'my':
            sorting = 'date_added'
            if request.user.username == "":
                raise Http404
            if tag != '':
                questions = Tag.objects.get(name=tag).question_set.filter(author=request.user.profile).order_by(sorting).reverse()
            else:
                questions = Question.objects.filter(author=request.user.profile).order_by(sorting).reverse()

        
        paginator = Paginator(questions, 5)

        try:
            currentpage = paginator.page(page)
        except EmptyPage:
            raise Http404
        except PageNotAnInteger:
            raise Http404
        return render(request, 'questions.html', {
            'tag' : tag,
            'pages': getPageDict(paginator, page),
            'questions' : paginator.page(page),
        })

    sort = request.REQUEST.get('sort', 'new')
    questions = Question.objects.order_by("date_added").reverse()
    tag_name = ""

    paginator = Paginator(questions, 5)
    try:
	currentpage = paginator.page(1)
    except EmptyPage:
	raise Http404
    except PageNotAnInteger:
	raise Http404
    return render(request, 'index.html', {
	'tag' : tag,
        'pages': getPageDict(paginator, 1),
	'questions' : paginator.page(1),
	'best_tags': Tag.objects.all().order_by('rating').reverse()[:10],
	'best_users': Profile.objects.all().order_by('rating').reverse()[:10],
	'sort': sort,
    })

@login_required
def profile(request):
    user = Profile.objects.get(user=request.user)
    return render(request, 'profile.html', {
	'best_tags': Tag.objects.all().order_by('rating').reverse()[:10],
	'best_users': Profile.objects.all().order_by('rating').reverse()[:10],
	'currentuser': user,
    })

def has_like(request):
    if request.user.username != "":

        if not request.is_ajax():
            raise Http404
	
	question_id = request.REQUEST.get('id')
	
	is_liked = 0

	try:
            like = Question_like.objects.get(author=request.user.profile, question=question_id)

	    if like.is_deleted == 1:
	        is_liked = 0
	    else:
	        is_liked = 1

	except ObjectDoesNotExist:
	    is_liked = 0
	
        return AjaxResponse(True, 'Success!', {'is_liked': is_liked})
    return AjaxResponse(False, 'Error!', {'error': 'user_not_auth'})

def like(request):
    if request.user.username != "":

        question = request.REQUEST.get('id')

	try:
	    q = Question.objects.get(pk=question)
	except ObjectDoesNotExist:
	    return AjaxResponse(False, 'Error!', {'error': 'question_not_exists'})

        if not request.is_ajax():
            raise Http404
	
        try:
            like = Question_like.objects.get(author=request.user.profile, question_id=question)
	    if like.is_deleted:
		like.is_deleted = False
		like.save()
		q.rating += 1
		q.save()
		return AjaxResponse(True, 'Success!', {'type': 'like'})
	    else:
		like.is_deleted = True
		like.save()
		q.rating -= 1
		q.save()
		return AjaxResponse(True, 'Success!', {'type': 'dislike'})		

        except ObjectDoesNotExist:
	    Question_like.objects.create(author=request.user.profile, question_id=question, is_deleted=False)
	    q.rating += 1
	    q.save()
	    return AjaxResponse(True, 'Success!', {'type': 'like'})

	return AjaxResponse(False, 'Error!', {'error': 'unknown_error'})

    return AjaxResponse(False, 'Error!', {'error': 'user_not_auth'})

def ask_question(request):
    if request.user.username != "":
        if request.method == "POST":
            form = QuestionForm(request.POST)
            if form.is_valid():
                tags_str = form.cleaned_data['tags']

                tags_names = tags_str.split(',')
                q = Question.objects.create(author=request.user.profile, title=form.cleaned_data['title'], text=request.POST['text'])
                for tag_name in tags_names:
                    try:
                        tag = Tag.objects.get(name=tag_name)
                        tag.rating += 1
                    except (ObjectDoesNotExist):
                        tag = Tag.objects.create(name=tag_name, rating=1)
                    q.tags.add(tag)
                return redirect('/question/' + str(q.id))
            else:
                return render(request, 'ask.html', {
			'best_tags': Tag.objects.all().order_by('rating').reverse()[:10],
			'best_users': Profile.objects.all().order_by('rating').reverse()[:10],
                    })
        else:
            return render(request, 'ask.html', {
                	'best_tags': Tag.objects.all().order_by('rating').reverse()[:10],
			'best_users': Profile.objects.all().order_by('rating').reverse()[:10],
                })
    else:
        return redirect('/accounts/login?next=/ask/')

@csrf_protect
def registration(request):
    if request.user.username == "":
        if request.method == "POST":
            username = request.POST['login']
            password = request.POST['password']
            email = request.POST['email']
            form = RegistrationForm(request.POST)
            try:
                if form.is_valid():
                        u = User.objects.create_user(username, email, password)
                        Profile.objects.create(user=u, avalar_url='/uploads/default_avatar.png')
                        user = authenticate(username=username, password=password)
                        login(request, user)
                        return redirect('/')
                else:
                    return render(request, 'signin.html', {
                        'best_tags': Tag.objects.all().order_by('rating').reverse()[:10],
			'best_users': Profile.objects.all().order_by('rating').reverse()[:10],
			'form': form,
			'password': password,
			'message': 'Invalid form',
                        })
            except IntegrityError:
                return render(request, 'signin.html', {
                    'best_tags': Tag.objects.all().order_by('rating').reverse()[:10],
		    'best_users': Profile.objects.all().order_by('rating').reverse()[:10],
		    'form': form,
                    'message': 'Duplicate login!'
                    })
        else:
            return render(request, 'signin.html', {
                'best_tags': Tag.objects.all().order_by('rating').reverse()[:10],
		'best_users': Profile.objects.all().order_by('rating').reverse()[:10],
                })
    else:
        return redirect('/')

def login_view(request):
    next = request.REQUEST.get('next', '/')
    if request.user.username == "":	
        if request.method == "POST":
            user = authenticate(username=request.POST['login'], password=request.POST['password'])
            if user is not None:
                login(request, user)
		return HttpResponseRedirect(next) 
            else:
                return render(request, 'login.html', {
                    'best_tags': Tag.objects.all().order_by('rating').reverse()[:10],
		    'best_users': Profile.objects.all().order_by('rating').reverse()[:10],
                    'message': 'Your username and password did not match. Please try again.',
                    })
        else:
            return render(request, 'login.html', {
                'best_tags': Tag.objects.all().order_by('rating').reverse()[:10],
		'best_users': Profile.objects.all().order_by('rating').reverse()[:10],
		'next' : next,
                })
    else:
        return redirect(next)

def helloworld(request):    
	return HttpResponse("Hello world!")

@csrf_exempt
def getpost(request):
	
	if (request.method == "GET"):  	
		get_dict = request.GET
    		get_body = ['%s=%s<br>' % (key, value) for key, value in get_dict.iteritems()]
		response_body = ['GET_PARAM:<br><br>'] + get_body

	if (request.method == "POST"):
    		post_dict = request.POST
    		post_body = ['%s=%s<br>' % (key, value) for key, value in post_dict.iteritems()]
	 	response_body = ['POST_PARAM:<br><br>'] + post_body
    	
	return HttpResponse(response_body)

