# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0004_question_like'),
    ]

    operations = [
        migrations.CreateModel(
            name='Answer_like',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('answer', models.ForeignKey(to='ask.Answer')),
                ('author', models.ForeignKey(to='ask.Profile')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
