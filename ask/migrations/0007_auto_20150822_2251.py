# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0006_auto_20150820_2212'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='answer_like',
            name='value',
        ),
        migrations.RemoveField(
            model_name='question_like',
            name='value',
        ),
        migrations.AddField(
            model_name='answer_like',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='question_like',
            name='is_deleted',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
