# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0005_answer_like'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='answer_like',
            name='is_deleted',
        ),
        migrations.RemoveField(
            model_name='question_like',
            name='is_deleted',
        ),
        migrations.AddField(
            model_name='answer_like',
            name='value',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='question_like',
            name='value',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
