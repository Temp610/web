# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ask', '0003_tag_rating'),
    ]

    operations = [
        migrations.CreateModel(
            name='Question_like',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('author', models.ForeignKey(to='ask.Profile')),
                ('question', models.ForeignKey(to='ask.Question')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
