from django.core.management.base import BaseCommand
from optparse import make_option
from ask.models import *
from django.contrib.auth.models import User
from django.db.models import Min, Max
from faker.frandom import random
from faker.lorem import sentence, sentences, words
from mixer.fakers import get_username, get_email, get_firstname, get_lastname
from pprint import pformat
from datetime import datetime, timedelta
import csv
import os

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--users',
            action='store',
            dest='users',
            default=0,
        ),
        make_option('--questions',
            action='store',
            dest='questions',
            default=0,
        ),
        make_option('--answers',
            action='store',
            dest='answers',
            default=0,
        ),
        make_option('--tags',
            action='store',
            dest='tags',
            default=0,
        ),
        make_option('--tagsforquestions',
            action='store',
            dest='tagsforquestions',
            default=0,
        ),
    )


    def handle(self, *args, **options):
        users = int(options['users'])
        questions = int(options['questions'])
        answers = int(options['answers'])
        tags = int(options['tags'])
        tagsforquestions = int(options['tagsforquestions'])

        if users != 0:
            usernames = {}
            while len(usernames) < users:
                usernames[get_username(length=30)] = 1
            userfile = open(os.path.dirname(os.path.realpath(__file__)) + '/users.csv', 'wb')
            userwriter = csv.writer(userfile, lineterminator='\n', quoting=csv.QUOTE_ALL)
            for username in usernames:
                userwriter.writerow([
                    0,						#id (auto increase)
                    'youshallnotpass',				#password
                    datetime.today().replace(microsecond=0),	#last login
                    0,						#is superuser
                    username,		
                    get_firstname(),	
                    get_lastname(),	
                    get_email(),	
                    0,						#is staff
                    0,						#is active
                    datetime.today().replace(microsecond=0)])	#date joined
	    
            profilefile = open(os.path.dirname(os.path.realpath(__file__)) + '/profiles.csv', 'wb')
            profilewriter = csv.writer(profilefile, lineterminator='\n', quoting=csv.QUOTE_ALL)
            for i in range(2, len(usernames) + 2):
                profilewriter.writerow([
                    0,					#id (auto increase)
                    random.randint(-50, 100),		#rating
                    '/uploads/default_avatar.png',	#avatar url
		    i])					#user id

        if questions != 0:
            questionfile = open(os.path.dirname(os.path.realpath(__file__)) + '/questions.csv', 'wb')
            questionwriter = csv.writer(questionfile, lineterminator='\n', quoting=csv.QUOTE_ALL)
            for i in range(0, questions):
                questionwriter.writerow([
                    0,						#id (auto increase)
                    (sentence())[:59],				#title
                    sentences(5),				#text
                    random.randint(-20, 100),			#rating
                    datetime.today().replace(microsecond=0),	#date added
                    random.randint(2, len(usernames) + 1)])	#author id

        if answers != 0:
            answerfile = open(os.path.dirname(os.path.realpath(__file__)) + '/answers.csv', 'wb')
            answerwriter = csv.writer(answerfile, lineterminator='\n', quoting=csv.QUOTE_ALL)
            for i in range(0, answers):
                answerwriter.writerow([
                    0,						#id (auto increase)
                    sentences(2),				#text
                    random.randint(-10, 40),			#rating
                    0,						#is right
                    datetime.today().replace(microsecond=0),	#date added
                    random.randint(2, len(usernames) + 1),	#author id
                    random.randint(1, questions)])		#question id

        if tags != 0:
	    tagnames = {}
            while len(tagnames) < tags:
                tagnames[(words(1))[0]] = 1	
            tagfile = open(os.path.dirname(os.path.realpath(__file__)) + '/tags.csv', 'wb')
            tagwriter = csv.writer(tagfile, lineterminator='\n', quoting=csv.QUOTE_ALL)
            for tagname in tagnames:
                tagwriter.writerow([
                    0,				#id (auto increase)
                    tagname,			#name	
                    random.randint(0, 500)])	#rating

        if tagsforquestions != 0:
            tfqfile = open(os.path.dirname(os.path.realpath(__file__)) + '/tagsforquestions.csv', 'wb')
            tfqwriter = csv.writer(tfqfile, lineterminator='\n', quoting=csv.QUOTE_ALL)
	    for i in range(1, questions + 1):
		tagids = {}
		tagcount = random.randint(1, tagsforquestions)
            	while len(tagids) < tagcount:
                    tagids[random.randint(1, len(tagnames))] = 1

                for tagid in tagids:
                    tfqwriter.writerow([
                        0,	#id (auto increase)
                        i,  	#question id
                        tagid])	#tag id
