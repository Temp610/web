from django.conf.urls import patterns, include, url
from django.contrib.auth.views import logout
from django.contrib import admin
from ask import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'ask_pupkin.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^getpost$', views.getpost, name = 'getpost'),
    url(r'^helloworld$', views.helloworld, name = 'helloworld'),
    url(r'^ask/$', views.ask_question, name = 'ask_question'),
    url(r'^accounts/login/?$', views.login_view, name = 'login'),
    url(r'^accounts/logout/$', logout, {'next_page' : '/'}, name = 'logout'),
    url(r'^accounts/profile/$', views.profile, name = 'profile'),
    url(r'^accounts/registration/$', views.registration, name = 'registration'),
    url(r'^tag/(((?P<tag_name>[^/]+)/)?)$', views.index),
    url(r'^question/(((?P<question_id>[^/]+)/)?)$', views.question),
    url(r'^/?$', views.index, { 'sort' : '' }),
    url(r'^hot/?$', views.index, { 'sort' : 'hot'}),
    url(r'^my/?$', views.index, { 'sort' : 'my'}),
    url(r'^user/(((?P<user_id>[^/]+)/)?)$', views.user),
    url(r'^api/has_like$', views.has_like),
    url(r'^api/like$', views.like),
)

