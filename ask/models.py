from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Profile(models.Model):
	user = models.OneToOneField(User)
	rating = models.IntegerField(default=0)
	avalar_url = models.CharField(max_length=60)

class Tag(models.Model):
	name = models.CharField(max_length=60)
	rating = models.IntegerField(default=0)

class Question(models.Model):
	author = models.ForeignKey(Profile)
	title = models.CharField(max_length=60)
	text = models.TextField()
	rating = models.IntegerField(default=0)
	date_added = models.DateTimeField(auto_now_add=True)
	tags = models.ManyToManyField(Tag)

class Answer(models.Model):
	author = models.ForeignKey(Profile)
	question = models.ForeignKey(Question)
	text = models.TextField()
	rating = models.IntegerField(default=0)
	is_right = models.BooleanField(default=False)
	date_added = models.DateTimeField(auto_now_add=True)

class Question_like(models.Model):
	question = models.ForeignKey(Question)
	author = models.ForeignKey(Profile)
	is_deleted = models.BooleanField(default=False)

class Answer_like(models.Model):
	answer = models.ForeignKey(Answer)
	author = models.ForeignKey(Profile)
	is_deleted = models.BooleanField(default=False)


