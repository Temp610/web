function question_load(id) {

	$.ajax({
		url: '/api/has_like?id=' + id,
		dataType: "json",
		success: function(data) {
			if (data.is_liked == 0) ;
			else if (data.is_liked == 1) $("#question_" + id).addClass('btn-liked');
			else if (data.error == 'user_not_auth') {
				$("#question_" + id).removeClass('btn');
				$("#question_" + id).addClass('badge');
			}
			else console.log('has_like_failure');
		}
	});

}

function question_like(id) {

	$.ajax({
		url: '/api/like?id=' + id,
		dataType: "json",
		success: function(data) {
                        if (data.status == true) {
				$("#question_" + id).toggleClass('btn-liked');
				var count = (parseInt($("#question_" + id).text().split(':')[1]));
				if (data.type == 'like') $("#question_" + id).text('like: ' + (count + 1));
				else if (data.type == 'dislike') $("#question_" + id).text('like: ' + (count - 1));
				else console.log('no_like_type');
			}
			else if (data.status == false) console.log(data.error);
			else alert('like_failure');
                }
	});
	
}

function questions_hot(page, tag) {


        if ( $("#content").data("tag") != tag ) {
                var nexturl = '/?page=1&sort=hot&tag=' + tag;
                var tag_html = 'Tag: ' + tag + '\n<a onclick="delete_tag();" class="btn btn-small">X</a>';
        }

        else {
                var nexturl = '/?page=' + page + '&sort=hot&tag=' + tag;
                var tag_html = 'Tag: ' + tag + '\n<a onclick="delete_tag();" class="btn btn-small">X</a>';
        }
        
        var prevPage = $("#content").data('page');

        $("#page_" + prevPage).removeClass("active");


	$.ajax({
                url: '/?page=' + page + '&sort=hot&tag=' + tag,
	        dataType: "html",
	        success: function(data) {
                        $("#content").animate({
                                'duration': 'fast',
                                'opacity': 0.001},
                                function(){
                                        $("#content").html(data);
                                        $("#content").data('page', page);
                                        $("#content").data('sorting', 'hot');
                                        $("#content").data('tag', tag);
                                        $("#nav_new").removeClass("active");
                                        $("#nav_my").removeClass("active");
                                        $("#nav_hot").addClass("active");

                                        if ( tag != "" ) {
                                                $("#tag_area").html(tag_html)
                                                $("#tag_area").show();
                                        }
                                        else {
                                                $("#tag_area").hide();
                                        }
                                        $("#page_" + page).addClass("active");

                                        $("body").animate({
                                                'duration': 100,
                                                'scrollTop': 0},
                                                function() {
                                                        $("#content").animate({
                                                                'opacity': 1,
                                                                'duration': 'fast'
                                                        });  
                                                }
                                        );
                                }
                        );
                }
	});
	
}

function questions_my(page, tag) {
        
        if ( $("#content").data("tag") != tag ) {
                var url = '/?page=1&sort=hot&tag=' + tag;
                var tag_html = 'Tag: ' + tag + '\n<a onclick="delete_tag();" class="btn btn-small">X</a>';
        }

        else {
                var url = '/?page=' + page + '&sort=hot&tag=' + tag;
                var tag_html = 'Tag: ' + tag + '\n<a onclick="delete_tag();" class="btn btn-small">X</a>';
        }
        
        var prevPage = $("#content").data('page');
        $("#page_" + prevPage).removeClass("active");

	$.ajax({
                
		url: '/?page=' + page + '&sort=my&tag=' + tag,
		dataType: "html",
		success: function(data) {
                        $("#content").animate({
                                'duration': 'fast',
                                'opacity': 0.001},
                                function(){
                                        $("#content").html(data);
                                        $("#content").data('page', page);
                                        $("#content").data('sorting', 'my');
                                        $("#content").data('tag', tag);
                                        $("#nav_new").removeClass("active");
                                        $("#nav_hot").removeClass("active");
                                        $("#nav_my").addClass("active");
                                        if ( tag != "" ) {
                                                $("#tag_area").html(tag_html)
                                                $("#tag_area").show();
                                        }
                                        else {
                                                $("#tag_area").hide();
                                        }
                                        $("#page_" + page).addClass("active");

                                        $("body").animate({
                                                'duration': 100,
                                                'scrollTop': 0},
                                                function() {
                                                        $("#content").animate({
                                                                'opacity': 1,
                                                                'duration': 'fast'
                                                        });  
                                                }
                                        );
                                }
                        );
                }
	});
	
}

function questions_new(page, tag) {

        if ( $("#content").data("tag") != tag ) {
                var url = '/?page=1&sort=hot&tag=' + tag;
                var tag_html = 'Tag: ' + tag + '\n<a onclick="delete_tag();" class="btn btn-small">X</a>';
        }

        else {
                var url = '/?page=' + page + '&sort=hot&tag=' + tag;
                var tag_html = 'Tag: ' + tag + '\n<a onclick="delete_tag();" class="btn btn-small">X</a>';
        }
        
        var prevPage = $("#content").data('page');
        $("#page_" + prevPage).removeClass("active");

	$.ajax({
		url: '/?page=' + page + '&sort=new&tag=' + tag,
		dataType: "html",
		success: function(data) {
                        $("#content").animate({
                                'duration': 'fast',
                                'opacity': 0.001},
                                function(){
                                        $("#content").html(data);
                                        $("#content").data('page', page);
                                        $("#content").data('sorting', 'new');
                                        $("#content").data('tag', tag);

                                        $("#nav_my").removeClass("active");
                                        $("#nav_hot").removeClass("active");
                                        $("#nav_new").addClass("active");
                                        if ( tag != "" ) {
                                                $("#tag_area").html(tag_html)
                                                $("#tag_area").show();
                                        }
                                        else {
                                                $("#tag_area").hide();
                                        }
                                        $("#page_" + page).addClass("active");

                                        $("body").animate({
                                                'duration': 100,
                                                'scrollTop': 0},
                                                function() {
                                                        $("#content").animate({
                                                                'opacity': 1,
                                                                'duration': 'fast'
                                                        });  
                                                }
                                        );
                                }
                        );
                }
	});
	
}

function delete_tag() {

        var page = $("#content").data("page");
        var sorting = $("#content").data("sorting");

        if ( sorting == 'new') questions_new(page, '');
        else if ( sorting == 'hot') questions_hot(page, '');
        else if ( sorting == 'my') questions_my(page, '');
        else alert('delete_tag!!');
	
}

function set_content_data() {
        
        $("#content").data('page', 1);
        $("#content").data('sorting', 'new');
        $("#content").data('tag', '');
        $("#page_1").addClass("active");
	
}

function get_page(i) {
        
        var tag = $("#content").data("tag");
        var sorting = $("#content").data("sorting");

        if ( sorting == 'new') questions_new(i, tag);
        else if ( sorting == 'hot') questions_hot(i, tag);
        else if ( sorting == 'my') questions_my(i, tag);
        else alert('get_page!!!');
	
}

function set_tag(tag) {
        
        var page = $("#content").data("page");
        var sorting = $("#content").data("sorting");

        if ( sorting == 'new') questions_new(page, tag);
        else if ( sorting == 'hot') questions_hot(page, tag);
        else if ( sorting == 'my') questions_my(page, tag);
        else alert('set_tag!!');
	
}

function getCurrentTag() {

        return $("#content").data('tag');
	
}

